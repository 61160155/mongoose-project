const mongoose = require('mongoose')
const Building = require('./models/building')
const Room = require('./models/room')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  const newInformaticsBuilding = await Building.findById('621b73780d5d7d6571cbb062')
  const room = await Room.findById('621b73780d5d7d6571cbb068')
  const informatics = await Building.findById(room.building)
  console.log(room)
  console.log('---------------------------------------------------------')
  console.log(newInformaticsBuilding)
  console.log('---------------------------------------------------------')
  console.log(informatics)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.room.push(room)
  informatics.room.pull(room)
  newInformaticsBuilding.save()
  room.save()
  informatics.save()
}

main().then(() => {
  console.log('finish')
})
