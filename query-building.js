const mongoose = require('mongoose')
const Building = require('./models/building')
const Room = require('./models/room')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // Update
//   const room = await Room.findById('621b6ecf4235ce9ac275efe3')
//   room.capacity = 20
//   room.save()
//   console.log(room)
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('---------------------------------------------------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const buildings = await Building.find({})
  console.log(JSON.stringify(buildings))
}

main().then(() => {
  console.log('finish')
})
